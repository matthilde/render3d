#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

typedef uint8_t u8;

/*
 * 3D RENDERER WRITTEN IN C BY MATTHILDE
 *
 * This is a 3D renderer written in about 250 lines of C.
 * It takes an .obj model and generates 360 images of it rotating.
 * Use the compile.sh script to generate an animation.
 *
 * NOTE: The code is messy but feel free to play around with it if
 *       you really want to.
 */

///// Graphics functions /////

// Few structs...
typedef struct point { int x, y; }     point;

typedef struct Color  { u8 r, g, b; } Color;
// The canvas struct
// w, h = width, height
// d    = distance between the plane and the origin (the eye) 
typedef struct Canvas { Color* cn; int w, h; float d; } Canvas;

// write canvas to PPM file
void ppm_write_file(const char* fn, Canvas* canvas) {
    FILE* f = fopen(fn, "w");
    Color c, *colors = canvas->cn;
    int w = canvas->w, h = canvas->h;

    fprintf(f, "P3\n%d %d\n255\n", w, h);
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x) {
            c = colors[y*w+x];
            fprintf(f, "%u %u %u ", c.r, c.g, c.b);
        }

    fclose(f);
}

// Clear the canvas
void clear_canvas(Canvas* cn) {
    for (int i = 0; i < cn->w * cn->h; ++i)
        cn->cn[i] = (Color){0, 0, 0};
}

// Plot a point with the color you want
void plot_point(Canvas *cn, point p, Color c) {
    int w = cn->w, h = cn->h;
    if (p.x >= 0 && p.x < w && p.y >= 0 && p.y < h)
        cn->cn[p.y*w+p.x] = c;
}

// Uses the Bresenham Algorithm to draw lines.
void plot_line(Canvas *cn, point p0, point p1, Color c) {
    int x0 = p0.x, y0 = p0.y, x1 = p1.x, y1 = p1.y;
    int dx = abs(x1 - x0), dy = abs(y1 - y0);
    int sx = x0 < x1 ? 1 : -1, sy = y0 < y1 ? 1 : -1;
    // int error = dx + dy, e2;
    int error = (dx > dy ? dx : -dy) / 2, e2;

    for (;;) {
        plot_point(cn, (point){ x0, y0 }, c);
        if (x0 == x1 && y0 == y1) break;
        e2 = error;
        if (e2 > -dx) {
            if (x0 == x1) break;
            error -= dy; x0 += sx;
        }
        if (e2 < dy) {
            if (y0 == y1) break;
            error += dx; y0 += sy;
        }
    }
}

// Plots a triangle
void plot_triangle(Canvas *cn, point p0, point p1, point p2, Color c) {
    plot_line(cn, p0, p1, c);
    plot_line(cn, p1, p2, c);
    plot_line(cn, p2, p0, c);
}

///// VECTOR STUFF /////

// Vector types
typedef struct vec3 { float x, y, z; } vec3;
typedef struct vec2 { float x, y; }    vec2;

// Vector arithmetic
vec3 vec3_mul(vec3 a, vec3 b) {
    vec3 nv;
    nv.x = a.x * b.x;
    nv.y = a.y * b.y;
    nv.z = a.z * b.z;
    return nv;
}
vec3 vec3_fmul(vec3 a, float b) {
    vec3 nv;
    nv.x = a.x * b;
    nv.y = a.y * b;
    nv.z = a.z * b;
    return nv;
}
vec3 vec3_add(vec3 a, vec3 b) {
    vec3 nv;
    nv.x = a.x + b.x;
    nv.y = a.y + b.y;
    nv.z = a.z + b.z;
    return nv;
}
vec3 vec3_sub(vec3 a, vec3 b) {
    vec3 nv;
    nv.x = a.x - b.x;
    nv.y = a.y - b.y;
    nv.z = a.z - b.z;
    return nv;
}

// Apply the rotation matrix on two floats
void apply_rotation(float *x, float *y, float angle) {
    float xx = *x, yy = *y;
    float mul = 3.1415 / 180;
    *x = xx * cos(angle * mul) - yy * sin(angle * mul);
    *y = xx * sin(angle * mul) + yy * cos(angle * mul);
}

// R o t a t e
vec3 vec3_rot_yaw(vec3 v, vec3 o, float angle) {
    vec3 t = vec3_sub(v, o);
    apply_rotation(&(t.x), &(t.z), angle);
    return vec3_add(t, o);
}

///// 3D STUFF /////

// Calculate the 2D coordinates of a 3D point.
int calculate_point(Canvas* cn, point *p, vec3 v) {
    float d = cn->d;
    if (v.z < d) return 0;

    vec2 np; point pnp; float m = d / v.z;
    int w = cn->w, h = cn->h;
    
    // Calculate coordinates
    np.x = v.x * m;
    np.y = v.y * m;

    pnp.x = (np.x * (w/2)) + (w/2);
    pnp.y = h - ((np.y * (h/2)) + (h/2));

    *p = pnp;
    return 1;
}
// Draw a 3D point
void draw_point(Canvas *cn, vec3 p) {
    point pnp; int w = cn->w, h = cn->h;
    
    if (!calculate_point(cn, &pnp, p)) return;
    if (pnp.x > 0 && pnp.x < w && pnp.y > 0 && pnp.y < h)
        plot_point(cn, pnp, (Color){ 255, 255, 255 });
}

/*
 * One of the biggest function of this code.
 *
 * Loads an Wavefront mesh and draws it on the canvas.
 * Note that it only supports triangulated meshes and you must make
 * use it has nothing else than just vertices and faces.
 * This format: f x0/y0/z0 x1/y1/z1... will probably cause the
 * program to do unexpected funnies.
 * It has to be this way: f v1 v2 v3
 *
 * .obj files can be opened in a text editor.
 * You can check it yourself fairly easily.
 */
// NOTE: It only support triangle faces.
void draw_obj_file(Canvas* cn, vec3 tf,
                   float m, float rot, const char* filename) {
    FILE *f = fopen(filename, "r");
    int c; char tmp[16]; vec3 p; int vsize = 0;
    point* vertex_points;

    //// FIRST PASS - Count Vertices in the mesh
    for (;;) {
        c = fscanf(f, "%s%*[^\n]\n", tmp);
        if (c == EOF) break;
        if (strcmp(tmp, "v") == 0) ++vsize;
    }
    // allocate points (the coordinates will be calculated on the fly)
    vertex_points = (point*)malloc(sizeof(point) * vsize);
    fseek(f, 0, SEEK_SET); // go back to the beginning.

    //// SECOND PASS - Populate the point array with 2D coordinates
    for (int i = 0; i < vsize;) {
        // first "command"
        c = fscanf(f, "%s", tmp);
        if (c == EOF) break;
        if (strcmp(tmp, "v") != 0) {
            // skip if it's not "v"
            fscanf(f, "%*[^\n]\n");
        } else {
            fscanf(f, "%f %f %f\n", &(p.x), &(p.y), &(p.z));
            // printf("%f %f %f\n", p.x, p.y, p.z);
            // Apply transform and rotation before calculating the
            // coordinates.
            p = vec3_rot_yaw(vec3_add(vec3_fmul(p, m), tf), tf, rot);
            if (!calculate_point(cn, vertex_points + i, p)) {
                // Abandon if a point cannot be seen (I know it's not
                // very clever but I am lazy)
                free(vertex_points); return;
            }
            ++i;
        }
    }

    // Go back to the beginning of the file again
    fseek(f, 0, SEEK_SET);

    //// THIRD PASS - Draw the faces
    for (;;) {
        c = fscanf(f, "%s", tmp);
        if (c == EOF) break;
        if (strcmp(tmp, "f") != 0) {
            fscanf(f, "%*[^\n]\n");
        } else {
            int a, b, c; point p0, p1; vec3 tmp;
            // Get the vertex indices in the "f" commands
            fscanf(f, "%d %d %d\n", &a, &b, &c);

            // Then draw triangle faces.
            plot_triangle(cn,
                          vertex_points[a-1],
                          vertex_points[b-1],
                          vertex_points[c-1],
                          (Color){255, 255, 255});
        }
    }

    // Free the array and close the file
    free(vertex_points);
    fclose(f);
}

///////////////

int main() {
    Color pixels[256*256];
    Canvas canvas = {
        .cn = pixels,
        .w = 256, .h = 256, .d = 1
    };

    vec3 transform = { -0.25, -0.5, 2.75 }; float mul = 1;

    char buf[256];
    for (int i = 0; i < 360; ++i) {
        clear_canvas(&canvas);
        draw_obj_file(&canvas, transform, mul, i, "monke.obj");
        sprintf(buf, "images/%04d.ppm", i);
        printf("%s...\r", buf); fflush(stdout);
        ppm_write_file(buf, &canvas);
    }

    return 0;
}
