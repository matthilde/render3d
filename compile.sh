#!/bin/sh
# Compilation script

rm -rf images
mkdir images

gcc -lm -O3 -g rendering.c -o rendering || exit
./rendering || exit
ffmpeg -f image2 -framerate 30 -i images/%04d.ppm -vf scale=256x256 test.gif
