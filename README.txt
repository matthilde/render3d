                       __                    __       __     
                      /\ \                 /'__`\    /\ \    
 _ __    __    ___    \_\ \     __   _ __ /\_\L\ \   \_\ \   
/\`'__\/'__`\/' _ `\  /'_` \  /'__`\/\`'__\/_/_\_<_  /'_` \   by matthilde
\ \ \//\  __//\ \/\ \/\ \L\ \/\  __/\ \ \/  /\ \L\ \/\ \L\ \ 
 \ \_\\ \____\ \_\ \_\ \___,_\ \____\\ \_\  \ \____/\ \___,_\
  \/_/ \/____/\/_/\/_/\/__,_ /\/____/ \/_/   \/___/  \/__,_ /

render3d is a very simple 3D renderer written in C in a few hours.
The goal of this project was just to make a tiny renderer to learn basic stuff
about 3D, don't expect something amazing.

DEPENDENCIES
------------

ffmpeg. that's it. and it's just to generate the spinny gif. (see compile.sh)

COMPILE.SH
----------

run this script and it will compile the program for you, run it, and compile
the rendered frames into a GIF using ffmpeg.

FEATURES
--------

 * outputs the render in PPM files
 * renders wireframe only
 * barebones .obj support
 * script that makes spinny gif

LICENSE
-------

I don't really care, do whatever you want with this code.
